"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var class_eleve_1 = require("./class_eleve");
// console.log("kevin");
// var nombre_a: number= 1;
// var nombre_b: number = 2;
// var nom:string = "Alex";
// var age:number = 37;
// function hello(alex: string, simion: number): string {
//     return `Bonjour, je m'appelle ${alex} et j'ai  ${simion}  ans.`;
// }
// console.log(hello(nom, age));
// var persons:string[] = ["Toto", "Titi", "John", "Paul"];
// function hello_friends(array_persons:string[]){
//     for(var i = 0;i<array_persons.length;i++) { 
//         console.log(`Salut ${persons[i]}`); 
//     }
// }
// hello_friends(persons);
// var adresse = {rue:"Balade", cp:"2A", ville:"Toulouse", appartement:103};
// function display_address(obj_adresse:{rue:string, cp:string|number, ville:string, appartement?:number}){
//     console.log(`J'habite à cette adresse : ${obj_adresse.rue} ${obj_adresse['cp']} ${obj_adresse['ville']} ${obj_adresse['appartement']}`); 
// }
// display_address(adresse);
var NS_Lang;
(function (NS_Lang) {
    var languages = /** @class */ (function () {
        function languages() {
        }
        languages.prototype.list = function () {
            var yourLanguages = ["RO", "FR", "ES", "EN"];
            return yourLanguages;
        };
        return languages;
    }());
    NS_Lang.languages = languages;
})(NS_Lang || (NS_Lang = {}));
var totoNotes = [2, 4, 3, 8, 9];
var totoAddress = display_address({ rue: "Capitol", cp: 15, ville: "Toulouse", appartement: 10 });
var toto = new class_eleve_1.Eleve("Toto", 34, totoAddress);
toto.note = totoNotes;
toto.addNote(5);
toto.addNote(6);
toto.addNote(7);
var knownLanguages = new NS_Lang.languages();
toto.languages = knownLanguages.list();
toto.notes();
console.log(toto);
function display_address(config) {
    var newAddress = { rue: config.rue, cp: config.cp, ville: config.ville, appartement: config.appartement };
    return newAddress;
}
