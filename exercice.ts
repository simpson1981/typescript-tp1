import {Eleve} from "./class_eleve";

// console.log("kevin");
// var nombre_a: number= 1;
// var nombre_b: number = 2;


// var nom:string = "Alex";
// var age:number = 37;

// function hello(alex: string, simion: number): string {
//     return `Bonjour, je m'appelle ${alex} et j'ai  ${simion}  ans.`;
    
// }

// console.log(hello(nom, age));

// var persons:string[] = ["Toto", "Titi", "John", "Paul"];

// function hello_friends(array_persons:string[]){

//     for(var i = 0;i<array_persons.length;i++) { 
//         console.log(`Salut ${persons[i]}`); 
//     }

// }

// hello_friends(persons);

// var adresse = {rue:"Balade", cp:"2A", ville:"Toulouse", appartement:103};

// function display_address(obj_adresse:{rue:string, cp:string|number, ville:string, appartement?:number}){
//     console.log(`J'habite à cette adresse : ${obj_adresse.rue} ${obj_adresse['cp']} ${obj_adresse['ville']} ${obj_adresse['appartement']}`); 
// }

// display_address(adresse);

namespace NS_Lang{
    export class languages{
        list(){
            let yourLanguages:Array<string> = ["RO", "FR", "ES", "EN"];

            return yourLanguages;
        }
    }
}


var totoNotes:Array<number> = [2, 4, 3, 8, 9];

var totoAddress = display_address({rue:"Capitol", cp: 15, ville: "Toulouse", appartement: 10});

var toto = new Eleve("Toto", 34, totoAddress);


toto.note = totoNotes;
toto.addNote(5);
toto.addNote(6);
toto.addNote(7);

var knownLanguages:NS_Lang.languages = new NS_Lang.languages();
toto.languages = knownLanguages.list();
toto.notes();

console.log(toto);



// Interfaces

export interface AddressDefinition{
    rue:string; 
    cp:number|string; 
    ville:string; 
    appartement:number;
}

function display_address(config: AddressDefinition ):AddressDefinition{

    let newAddress = {rue:config.rue, cp:config.cp, ville:config.ville, appartement:config.appartement}
    return newAddress;
}







