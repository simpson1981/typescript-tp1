"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Eleve = /** @class */ (function () {
    function Eleve(prenom_text, age_text, addresse_text) {
        this._note = [];
        this._languages = [];
        this._prenom = prenom_text;
        this._age = age_text;
        this._addresse = addresse_text;
    }
    Object.defineProperty(Eleve.prototype, "prenom", {
        get: function () {
            return this.prenom;
        },
        set: function (value) {
            this._prenom = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Eleve.prototype, "age", {
        get: function () {
            return this.age;
        },
        set: function (value) {
            this._age = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Eleve.prototype, "addresse", {
        get: function () {
            return this.addresse;
        },
        set: function (value) {
            this._addresse = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Eleve.prototype, "note", {
        get: function () {
            return this._note;
        },
        set: function (value) {
            this._note = value;
        },
        enumerable: true,
        configurable: true
    });
    Eleve.prototype.addNote = function (value) {
        if (value >= 0 && value <= 20) {
            this._note.push(value);
        }
        else {
            alert("The number must be between 0 and 20");
        }
    };
    Eleve.prototype.eleve_info = function (prenom, age, addresse) {
        return " " + prenom + " " + age + " " + addresse;
    };
    Eleve.prototype.calc_moyenne = function () {
        var note_sum = 0;
        for (var i = 0; i < this._note.length; i++) {
            note_sum += this._note[i];
        }
        return note_sum / this._note.length;
    };
    Eleve.prototype.notes = function () {
        for (var i = 0; i < this._note.length; i++) {
            console.log("La note: " + this._note[i]);
        }
        console.log("The student has the following notes: " + this._note);
        console.log("La moyenne des notes: " + this.calc_moyenne());
    };
    Object.defineProperty(Eleve.prototype, "languages", {
        get: function () {
            return this._languages;
        },
        set: function (value) {
            this._languages = value;
        },
        enumerable: true,
        configurable: true
    });
    return Eleve;
}());
exports.Eleve = Eleve;
