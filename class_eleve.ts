
import {AddressDefinition} from "./exercice";
export class Eleve {
    _prenom:string;
    _age:number; 
    _addresse:AddressDefinition;
    _note:Array<number> =[];
    _languages:Array<string> = [];
    

constructor(prenom_text:string, age_text:number, addresse_text:AddressDefinition){
        this._prenom = prenom_text;
        this._age = age_text;
        this._addresse = addresse_text;
        
    }

    get prenom():string{
        return this.prenom;
    }
    get age():number{
        return this.age;
    }
    get addresse():AddressDefinition{
        return this.addresse;
    }

    get note():Array<number>{
        return this._note;
    }

    set prenom(value:string){
        this._prenom = value;
    }

    set age(value:number){
        this._age = value;
    }
    set addresse(value:AddressDefinition){
        this._addresse = value;
    }

    addNote(value:number){
        if(value>=0 && value<=20){
            this._note.push(value);
        }else{
            alert("The number must be between 0 and 20");
        }  
    }

    set note(value:Array<number>){
        this._note = value;
    }


    eleve_info(prenom:string, age:number, addresse:object ):string{
        return ` ${prenom} ${age} ${addresse}`;
    }

    calc_moyenne():number{
        var note_sum:number = 0;
        for(let i = 0; i < this._note.length; i++){
            note_sum += this._note[i];
        } 

        return note_sum/this._note.length;
    }

    notes(){
        
        for(let i = 0; i < this._note.length; i++){
            console.log("La note: " + this._note[i]);
        } 
        console.log("The student has the following notes: " + this._note);
        console.log("La moyenne des notes: " + this.calc_moyenne());
    }

    set languages(value:Array<string>){
        this._languages = value;
    }

    get languages():Array<string>{
        return this._languages;
    }


}